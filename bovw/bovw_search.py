import cv2
import imutils 
import numpy as np
import os
from sklearn.externals import joblib
from scipy.cluster.vq import *
import csv
from sklearn import preprocessing
import numpy as np

from pylab import *
from PIL import Image

def getPaths(path):
    paths = []
    alist_filter = ['peg','jpg'] 
    for r,d,f in os.walk(path):
        for file in f:
            if file[-3:] in alist_filter:
                paths.append(os.path.join(r,file))
    return paths

def extract_features(image_path):
	# Create feature extraction and keypoint detector objects
    alg = cv2.xfeatures2d.SIFT_create()  
    img1 = cv2.imread(image_path, 0) 
    kp, des = alg.detectAndCompute(img1,None)
    return kp, des

def getMatch(qryImgPath):
	# List where all the descriptors are stored
	des_list = []
	
	kpts, des = extract_features(qryImgPath)
	
	des_list.append((qryImgPath, des))   
		
	# Stack all the descriptors vertically in a numpy array
	descriptors = des_list[0][1]
	
	# Load the classifier, class names, scaler, number of clusters and vocabulary 
	im_features, image_paths, idf, numWords, voc = joblib.load("bof.pkl")

	# 
	test_features = np.zeros((1, numWords), "float32")
	words, distance = vq(descriptors,voc)
	for w in words:
		test_features[0][w] += 1

	# Perform Tf-Idf vectorization and L2 normalization
	test_features = test_features*idf
	test_features = preprocessing.normalize(test_features, norm='l2')

	score = np.dot(test_features, im_features.T)
	print(score)
	rank_ID = np.argsort(-score)
	print(rank_ID)
	# Visualize the results

	print("Query: ", qryImgPath)
	pred = rank_ID[0][0]
	print("Prediction: ", image_paths[pred])

	return qryImgPath, image_paths[pred]

# Get query image dataset path
dirTest =  '/home/manfred/data/datasets/resources_wohnzimmer/wz_test'
#dirTest =  "/home/manfred/python/trad/dataset/train/"
dirTestList = sorted(os.listdir(dirTest))
testImgPaths = getPaths(dirTest)

GT = []
BOVW_Pred = []

for testImgPath in testImgPaths:

	"""_GT, _BOVW_Pred = getMatch(testImgPath)
	GT += [_GT]
	BOVW_Pred += [_BOVW_Pred]"""
	GT += [getMatch(testImgPath)]

# write results to csv
with open('results_bovw.csv', 'w', newline='') as csvfile:
	writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
	for match in GT:
		writer.writerow(match)

print("halt.")

