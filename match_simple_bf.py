import numpy as np
import cv2
import sys, os
import matplotlib.pyplot as plt
from scipy.misc import imread

print(cv2.__version__)

def getPaths(path):
    paths = []
    alist_filter = ['peg','jpg'] 
    for r,d,f in os.walk(path):
        for file in f:
            if file[-3:] in alist_filter:
                paths.append(os.path.join(r,file))
    return paths

dirTest =  '/home/manfred/data/datasets/resources_wohnzimmer/wz_test'
dirTrain = '/home/manfred/data/datasets/resources_wohnzimmer/wz_train'
dirTrainList = sorted(os.listdir(dirTrain))
dirTestList = sorted(os.listdir(dirTest))

trainFiles = getPaths(dirTrain)
testFiles = getPaths(dirTest)

alg = cv2.xfeatures2d.SIFT_create()

img1 = imread(testFiles[2],mode="RGB")
kp1, des1 = alg.detectAndCompute(img1,None)

bf = cv2.BFMatcher()

for file in trainFiles:
   
    img2 = imread(file,mode="RGB")
    kp2, des2 = alg.detectAndCompute(img2,None)

    print('KP Img1: ', str(len(kp1)))
    print('KP Img2: ', str(len(kp2)))

    matches = bf.knnMatch(des1,des2, k=2)

    draw_params = dict(matchColor = (0,255,0),
                         singlePointColor = (255,0,0),
                         flags = 0)

    good = []
    for m,n in matches:
        if m.distance < 0.75*n.distance:
            good.append([m])

    good.sort(key=lambda x: x[0].distance)
    print('KP after ratio test: ', len(good))

    num_kp = 0
    if len(kp1) <= len(kp2):
        num_kp = len(kp1)
    else:
        num_kp = len(kp2)

    score = len(good) / num_kp * 100
    print('Similarity of Image ' + os.path.basename(file) + ': ' + str(score) + '\n' )

    img3 = cv2.drawMatchesKnn(img1,kp1,img2,kp2,good,None,**draw_params)
    plt.imshow(img3)
    plt.show()