import numpy as np
import cv2
import sys, os
import matplotlib.pyplot as plt
from scipy.misc import imread
from operator import itemgetter
import _pickle as pickle
import random
import csv

print(cv2.__version__)

def pickle_keypoints(keypoints, descriptors):
    i = 0
    temp_array = []
    for point in keypoints:
        temp = (point.pt, point.size, point.angle, point.response, point.octave,
        point.class_id, descriptors[i])     
        i = i + 1
        temp_array.append(temp)
    return temp_array

def unpickle_keypoints(array):
    keypoints = []
    descriptors = []
    for point in array:
        temp_feature = cv2.KeyPoint(x=point[0][0],y=point[0][1],_size=point[1], _angle=point[2], _response=point[3], _octave=point[4], _class_id=point[5])
        temp_descriptor = point[6]
        keypoints.append(temp_feature)
        descriptors.append(temp_descriptor)
    return keypoints, np.array(descriptors)

def extract_features(image_path):
    alg = cv2.xfeatures2d.SIFT_create()  
    img1 = cv2.imread(image_path, 0) 
    kp, des = alg.detectAndCompute(img1,None)
    return kp, des

def batch_extractor(imagePaths, pickled_db_path="match_flann_homography.pck"):
    result = {}
    temp_array = []
       
    """files = [os.path.join(images_path, p) for p in sorted(os.listdir(images_path))]

    # Filtering the Directories
    for file in files: 
        if os.path.isdir(file):
            files.remove(file) """

    # Iterating the File-Path Array
    for f in imagePaths:
        print('Extracting features from image %s' % f)
        name = f.split('/')[-1].lower()
        #result[name] = extract_features(f)
        kp, des = extract_features(f)
        temp = pickle_keypoints(kp, des)
        #temp_array.append(temp)
        #result[name] = temp
        result[f] = temp

    
    # saving all in pickled file
    with open(pickled_db_path, 'wb') as wfp:
        pickle.dump(result, wfp)
        #pickle.dump(temp_array, wfp)


def getPaths(path):
    paths = []
    alist_filter = ['peg','jpg'] 
    for r,d,f in os.walk(path):
        for file in f:
            if file[-3:] in alist_filter:
                paths.append(os.path.join(r,file))
    return paths



def getMatch(qryImgPath, pickled_db_path="match_flann_homography.pck"):
    # Extract the Keypoints an Descriptors
    #qrykp, qrydes = extract_features(qry_img[0])
    qrykp, qrydes = extract_features(qryImgPath)

    # re-load the database
    with open(pickled_db_path, 'rb') as fp:
        data = pickle.load(fp)

    names = []
    kp = []
    des = []
    
    for n, _kp_des in data.items():
        names.append(n)
        tmp_kp, tmp_des = unpickle_keypoints(_kp_des)
        kp.append(tmp_kp)
        des.append(tmp_des)
    
    des = np.array(des)
    qrydes = np.array(qrydes)

    #print(str(names[0])+"\n"+str(kp[0])+"\n"+str(des[0])) 
    print ("halt")
    
    y_true = []
    y_pred_ratio = []
    y_pred_hom = []


    # Qry-Bild 
    print("Query Image: ", qryImgPath)
    y_true.append(qryImgPath)

    
    tmp_pred_ratio = []
    tmp_pred_hom = []

    # FLANN parameters ----------------------------------------------
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)   # or pass empty dictionary

    flann = cv2.FlannBasedMatcher(index_params,search_params)

    for i in range(len(names)):
        print('Matching features from image %s' % names[i])

        matches = flann.knnMatch(qrydes,des[i],k=2)
        print(len(matches))

        #--------------------------------

        # LOEWE Ratio-Test ------------------------
        good = []
        for m, _n in matches:
            if m.distance < 0.7*_n.distance:
                good.append(m)

        good.sort(key=lambda x: x.distance)
        
        print('Matches after ratio test: ', len(good))
        tmp_pred_ratio.append([names[i], len(good)])

        if len(good) > 1:
            # Homography ------------------------------
            query_pts = np.float32([qrykp[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
            train_pts = np.float32([kp[i][m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

            matrix, mask = cv2.findHomography(query_pts, train_pts, cv2.RANSAC, 5.0)
            matchesMask = mask.ravel().tolist()
                
            # Counting the 1 in matchesMask
            npMatchesMask = np.array(matchesMask)
            num_matchesMask  = (npMatchesMask == 1).sum()
            
            print('Matches in the Mask: ', num_matchesMask)
            tmp_pred_hom.append([names[i], num_matchesMask])

    
    tmp_pred_ratio = sorted(tmp_pred_ratio, key=itemgetter(1), reverse=True)
    tmp_pred_hom = sorted(tmp_pred_hom, key=itemgetter(1), reverse=True)
    y_pred_ratio.append(tmp_pred_ratio[0][0])
    y_pred_hom.append(tmp_pred_hom[0][0])
      
    """
    print("True: ")
    print(y_true)
    print("-------------------")
    print("Pred LOEWE Ratio Test: ")
    print(y_pred_ratio)
    print("Pred LOEWE Ratio Test + Homography: ")
    print(y_pred_hom)"""
    
    # Return Qry-Image, Pred-Ratio, Pred-Hom
    return y_true, y_pred_ratio, y_pred_hom 
    

def run():

    #dirTest =  '/home/manfred/data/datasets/resources_wohnzimmer_testing/wz_test'
    #dirTrain = '/home/manfred/data/datasets/resources_wohnzimmer_testing/wz_train'
    dirTest =  '/home/manfred/data/datasets/resources_wohnzimmer/wz_test'
    dirTrain = '/home/manfred/data/datasets/resources_wohnzimmer/wz_train'

    dirTrainList = sorted(os.listdir(dirTrain))
    dirTestList = sorted(os.listdir(dirTest))

    trainFilePaths = getPaths(dirTrain)
    testFilePaths = getPaths(dirTest)

    """# extract all the image descriptor to pickle file    
    for d in dirTrainList:
        images_path = os.path.join(dirTrain,d)
        files = [os.path.join(images_path, p) for p in sorted(os.listdir(images_path))]
        batch_extractor(images_path)"""
   
    batch_extractor(trainFilePaths)
    
    GT = []

    for testfilePath in testFilePaths:
        GT += [getMatch(testfilePath)] 
    

    # write results to csv
    with open('results_flann.csv', 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        for match in GT:
            writer.writerow(match)


run()