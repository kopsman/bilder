import numpy as np
import cv2
import sys, os
import matplotlib.pyplot as plt
from scipy.misc import imread
from operator import itemgetter
import _pickle as pickle
import random
from sklearn.metrics import accuracy_score, precision_score, auc

print(cv2.__version__)

def extract_features(image_path):
    alg = cv2.xfeatures2d.SIFT_create()  
    img1 = cv2.imread(image_path, 0) 
    kp, des = alg.detectAndCompute(img1,None)
    return kp, des


def getPaths(path):
    paths = []
    alist_filter = ['peg','jpg'] 
    for r,d,f in os.walk(path):
        for file in f:
            if file[-3:] in alist_filter:
                paths.append(os.path.join(r,file))
    return paths


def getMatch(testFiles, trainFiles):
    
    y_true = []
    y_pred_ratio = []
    y_pred_hom = []

    for qry_img in testFiles:

        # Extract the Keypoints an Descriptors
        qrykp, _qrydes = extract_features(qry_img)
        qrydes = np.array(_qrydes)

        #print(str(names[0])+"\n"+str(kp[0])+"\n"+str(des[0])) 
        print ("halt")
        
        # noch anders benennen
        tmp_pred_ratio = []
        tmp_pred_hom = []

        # Qry-Bild 
        print("Query Image: ", qry_img)
        y_true.append(qry_img)

        # FLANN parameters ----------------------------------------------
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks=50)   # or pass empty dictionary
        flann = cv2.FlannBasedMatcher(index_params,search_params)

        for file in trainFiles:


            kp2, des2 = extract_features(file)

            print('KP Img1: ', str(len(qrykp)))
            print('KP Img2: ', str(len(kp2)))
            
            matches = flann.knnMatch(qrydes,des2,k=2)
            print(len(matches))

            good = []
            for m, n in matches:
                if m.distance < 0.6*n.distance:
                    good.append(m)
            good.sort(key=lambda x: x.distance)
                
            print('Matches after ratio test: ', len(good))
            tmp_pred_ratio.append([file, len(good)])
            
            if len(good) > 1:
                # Homography ------------------------------
                query_pts = np.float32([qrykp[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
                train_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

                matrix, mask = cv2.findHomography(query_pts, train_pts, cv2.RANSAC, 5.0)
                matchesMask = mask.ravel().tolist()
                    
                # Counting the 1 in matchesMask
                npMatchesMask = np.array(matchesMask)
                num_matchesMask  = (npMatchesMask == 1).sum()
                
                print('Matches in the Mask: ', num_matchesMask)
                tmp_pred_hom.append([file, num_matchesMask])

        
        tmp_pred_ratio = sorted(tmp_pred_ratio, key=itemgetter(1), reverse=True)
        tmp_pred_hom = sorted(tmp_pred_hom, key=itemgetter(1), reverse=True)

        y_pred_ratio.append(tmp_pred_ratio[0][0])
        y_pred_hom.append(tmp_pred_hom[0][0])
        #Return Qry-Image, Pred-Ratio, Pred-Hom
        #return qry_img, 
        print("True: ")
        print(y_true)
        print("-------------------")
        print("Pred LOEWE Ratio Test: ")
        print(y_pred_ratio)
        print("Pred LOEWE Ratio Test + Homography: ")
        print(y_pred_hom)
        print("-------------------")
        print("-------------------")
    
    print("end")
    return y_true, y_pred_ratio, y_pred_hom 

def run():

    dirTest =  '/home/manfred/data/datasets/resources_wohnzimmer_testing/wz_test'
    dirTrain = '/home/manfred/data/datasets/resources_wohnzimmer_testing/wz_train'
    #dirTest =  '/home/manfred/data/datasets/resources_wohnzimmer/wz_test'
    #dirTrain = '/home/manfred/data/datasets/resources_wohnzimmer/wz_train'

    dirTrainList = sorted(os.listdir(dirTrain))
    dirTestList = sorted(os.listdir(dirTest))

    trainFiles = getPaths(dirTrain)
    testFiles = getPaths(dirTest)

    # getting 1 random image from the test set 
    testFiles=getPaths(dirTest)
    print('------------------------------\n')
    #sample = random.sample(testFiles, 1)
    #sample = testFiles

    GT, RATIO, HOM = getMatch(testFiles, trainFiles)
    
    """
    print(accuracy_score(GT, RATIO))
    print(accuracy_score(GT, HOM))
    print("---------------------")

    print(precision_score(GT, RATIO,average='macro'))
    print(precision_score(GT, HOM, average='macro'))
    print("---------------------")
    
    print(auc(GT, RATIO))
    print(auc(GT, HOM))
    print("---------------------")
    
    print("Ende")"""
    #-----------------------------------------------------



run()